
public class ParseTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ProjectConverter projectConverter = new ProjectConverter();
		DocReader reader = new DocReader(projectConverter, "projects.xml");
		reader.build();
		reader.print();
	}
	
}