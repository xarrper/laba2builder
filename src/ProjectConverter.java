import java.util.ArrayList;

public class ProjectConverter extends AbstractConverter {

	private Project project;
	
	public ProjectConverter(){
		this.project = new Project();
	}
	
	public void readDept(String dept) {
		project.setDept(dept);
	}

	public void readProject(String projectName) {
		project.setProject(projectName);	
	}

	@Override
	public void readGroup(String group) {
		project.setGroup(group);	
	}

	@Override
	public void readSupervisor(String supervisorName) {
		project.setSupervisor(supervisorName);		
	}

	@Override
	public void readEmail(String email) {
		project.setEmail(email);
	}

	@Override
	public void readDescription(String description) {
		project.setDescription(description);
	}
	
	public Project getProject() {
		return project;
	}
	
}
