
public interface NodeConverter {
	
	public void readDept(String dept);
	public void readProject(String projectName);
	public void readGroup(String group);
	public void readSupervisor(String supervisorName);
	public void readEmail(String email);
	public void readDescription(String description);
	public Project getProject();

}