
public class Project {

	String dept;
	String project;
	String group;
	String email;
	String supervisor;
	String description;
	
	public Project() {
		
	}
	
	public Project(String dept, String project, String group, String email, String supervisor, String description) {
		this.dept = dept;
		this.project = project;
		this.group = group;
		this.email = email;
		this.supervisor = supervisor;
		this.description = description;
	}
	@Override
	public String toString() {
		return "Project [dept=" + dept + ", project=" + project + ", group="
				+ group + ", email=" + email + ", supervisor=" + supervisor
				+ ", description=" + description + "]";
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
